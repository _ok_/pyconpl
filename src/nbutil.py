from .log import setup_logging, logging
from .filestore import filestore
from functools import reduce, partial
import time

def set_output(rule, *values):
    """Emulate hard work by sleeping, and set rule outputs to values"""
    time.sleep(0.3)
    for key, value in zip(rule.outputs, values):
        logging.info('%s = %s', key, value)
        filestore[key] = value

def reduce_recipe(op):
    """Return rule recipe, which sets rule output like this:
    output = input1 `op` input2 `op` ... `op` inputN
    """
    return lambda rule: lambda: set_output(rule,
        reduce(op, (filestore.get(key) for key in rule.inputs))
    )
