from .filestore import filestore

from math import inf
import sys
import logging
from shlex import quote

def join_quoted(keys):
    return ' '.join(quote(str(key)) for key in keys)

def default_name(rule):
    return join_quoted(rule.outputs) if rule.outputs else "<PHONY>"

class Rule:

    def __init__(self, recipe, parents=(), inputs=(), outputs=(),
                 name=default_name):
        """Create rule with given parents, inputs and outputs.
        name -- rule name to be used for logging.
        If name is None, it'll be derived from rule outputs.
        recipe -- function which takes this rule
        and returns coroutine or a function to call when rule is computed.
        E.g.: recipe=lambda rule: lambda: print(rule.inputs, rule.outputs)
        """
        self.parents = tuple(parents)
        self.inputs = tuple(inputs)
        self.outputs = set(outputs(self) if callable(outputs) else outputs)
        self.name = name(self) if callable(name) else name
        self.recipe = recipe(self)

    def __repr__(self):
        return self.name

    def all_exist(self, keys):
        return all(key in filestore for key in keys)

    def up_to_date(self):
        """Rule is up to date if all its inputs and outputs exist,
        and every input is older than every output.
        """

        if not self.inputs:
            return self.all_exist(self.outputs)
        if not self.outputs:
            return self.all_exist(self.inputs)
        return (
            max(filestore.mtime(key) or inf for key in self.inputs) <=
            min(filestore.mtime(key) or -inf for key in self.outputs)
        )

    class Failed(Exception):
        """Exception class for Rule"""
        def __init__(self, message):
            self.message = message

    def before_recipe(self):
        """Fail if any input is missing"""
        missing = [key for key in self.inputs if key not in filestore]
        if missing:
            raise Rule.Failed(f"Missing inputs: {missing}")

    def after_recipe(self):
        """Fail if rule is not up to date"""
        if not self.up_to_date():
            raise Rule.Failed(
                f'Inputs changed, or some outputs not produced: {self.outputs}')

    def clean(self, on_exception=False):
        """Remove rule outputs from the store.
        If on_exception is True, also log last exception
        """
        if on_exception:
            logging.info(f'%s: Cleanup after %s', self, sys.exc_info()[0])
        for key in self.outputs:
            filestore.remove(key)

    @property
    def output(self):
        """Return one of the rule's outputs"""
        return next(iter(self.outputs))

def child(parents, **kwargs):
    """Return child rule which inputs are parents' outputs"""
    return Rule(
        parents=parents,
        inputs=tuple(key for p in parents for key in p.outputs),
        **kwargs,
    )
