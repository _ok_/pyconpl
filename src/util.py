from .rule import filestore, Rule, child, join_quoted

import logging
import asyncio
import sys
from shlex import quote
from itertools import product
from functools import partial
from pathlib import Path
import os

def cached(cache={}):
    """If rule with same outputs already exists in cache, return existing one.
    Otherwise, add rule to cache.
    Not thread-safe!
    """
    def func(rule):
        key = tuple(rule.outputs)
        if key not in cache:
            cache[key] = rule
        return cache[key]
    return func

async def async_sh(cmd_string, prefix=''):
    logging.info(cmd_string)
    proc = await asyncio.create_subprocess_exec(
        'bash', '-o', 'pipefail', '-c', cmd_string,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    stdout, stderr = await proc.communicate()
    if stdout:
        sys.stdout.write(stdout)
    if stderr:
        logging.warning('%s: %s', prefix, stderr.decode())
    if proc.returncode:
        raise Rule.Failed(f'Subprocess exited with status {proc.returncode}')

def sh(cmd, run=async_sh):
    """Recipe to run command asynchronously in shell"""
    return lambda rule: partial(run,
        cmd.format(
            inputs=join_quoted(rule.inputs),
            output=join_quoted(rule.outputs),
        ),
        prefix=rule.name
    )

def identity(x):
    return x

def square_of_sum(
    varz,
    var_recipe,
    mul_recipe,
    sum_recipe,
    rule_mod=identity,
    var_out=identity,
    mul_out=identity,
    sum_out=identity,
):
    varz = list(varz)
    logging.info('Var values:\t%s', varz)
    var_rules = [
        rule_mod(Rule(
            outputs={var_out(key)},
            recipe=var_recipe(val),
        ))
        for key, val in varz
    ]
    logging.info('Var rules:\t%s', var_rules)

    term_rules = [
        rule_mod(child(
            parents=factors,
            outputs=lambda rule: {mul_out(
                "⋅".join(map(os.path.basename, rule.inputs))
            )},
            recipe=mul_recipe,
        )) for factors in product(var_rules, var_rules)
    ]
    logging.info('Term rules:\t%s', term_rules)

    total = child(
        parents=tuple(term_rules),
        outputs=lambda rule: {sum_out(
            "(" + ' + '.join(sorted(os.path.basename(var.output) for var in var_rules)) + ")²"
        )},
        recipe=sum_recipe,
    )
    logging.info('Total rule:\t%s', total)
    return [*var_rules, *term_rules, total]
