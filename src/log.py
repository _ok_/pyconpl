# import logging
import logging.config

def setup_logging(level='DEBUG'):
    logging.config.dictConfig({
        'version': 1,
        'formatters': {
            'default': {
                'format': '%(asctime)s\t%(name)s\t%(levelname)s\t%(message)s'
            },
            'console': {
                'format': '%(message)s'
            },
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'level': level,
                'formatter': 'console',
            },
            'file': {
                'class': 'logging.FileHandler',
                'filename': 'task.log',
                'level': level,
                'mode': 'a',
                'formatter': 'default',
            },
        },
        'root': {
            'level': level,
            'handlers': ['console', 'file']
        },
    })
