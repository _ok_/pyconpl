from .rule import (Rule, filestore)

from multiprocessing import cpu_count
from concurrent.futures import ThreadPoolExecutor
from traceback import format_exc
from functools import partial
import threading

import collections
import asyncio
import logging
import os
import sys

class Computation:
    """Parallel computation of rules"""

    def __init__(self, rules, njobs=cpu_count()):
        """Initialize computation with rules
        njobs -- number of jobs to run in parallel.
        """
        self.rules = set(rules) # order doesn't matter
        self.njobs = njobs

    def compute(self):
        """Compute self.rules in asyncio event loop in new thread.
        (not the main thread because it may already have its own event loop)
        """
        loop = asyncio.new_event_loop()
        asyncio.get_child_watcher().attach_loop(loop)
        with ThreadPoolExecutor(1) as executor:
            thread_fut = executor.submit(partial(self.run_loop, loop))
            try:
                return thread_fut.result()
            except KeyboardInterrupt:
                loop.call_soon_threadsafe(self.cancel_all_and_wait)
                return thread_fut.result()

    def run_loop(self, loop):
        """Start all tasks in this event loop, and run until completion.
        Return list of (rule, result) tuples, where result can be exception.
        """
        asyncio.set_event_loop(loop)
        self.semaphored = self.init_semaphore()
        # Each rule will wait() for its parents to set() the completion event.
        self.events = {rule: asyncio.Event() for rule in self.rules}
        # Launch all the rules at once
        self.tasks = [asyncio.ensure_future(self.rule_task(rule)) for rule in self.rules]
        self.gather_all = asyncio.gather(*self.tasks, return_exceptions=True)
        return list(zip(self.rules, loop.run_until_complete(self.gather_all)))

    async def rule_task(self, rule):
        """Wait for parents to complete,
        compute the rule and notify children when it's completed.
        On computation failure or other exception, cancel all children tasks.
        """
        try:
            parents_completed = await self.wait_for_parents(rule)
            if not parents_completed:
                logging.debug('%s: Parents cancelled', rule)
                self.cancel_children(rule)
                return
            logging.debug('%s: Parents completed', rule)
            await self.compute_rule(rule)
            self.events[rule].set()
            return True
        except Rule.Failed as e:
            logging.error('%s failed: %s', rule, e.message)
            self.cancel_children(rule)
            return
        except:  # something unexpected happened
            logging.error('%s: %s', rule, sys.exc_info()[:1])
            logging.error('%s', format_exc(chain=True))
            self.cancel_children(rule)
            raise

    async def compute_rule(self, rule):
        """Compute the rule, raise Failed on failure"""
        if rule.up_to_date():
            return
        rule.before_recipe()
        try:
            res = rule.recipe()
            if asyncio.iscoroutine(res):
                await self.semaphored(res)
        except:
            rule.clean(on_exception=True)
            raise
        rule.after_recipe()

    async def wait_for_parents(self, rule):
        """Wait for parents to complete.
        If while waiting the rule was cancelled, cancel children.
        Return None if cancelled, otherwise return True.
        """
        try:
            await asyncio.gather(*(self.events[p].wait() for p in rule.parents))
            return True
        except asyncio.CancelledError:
            self.cancel_children(rule)

    def cancel_children(self, rule):
        """Cancel all tasks waiting for rule to complete"""
        for task in self.events[rule]._waiters:
            task.cancel()

    def cancel_all_and_wait(self):
        for task in self.tasks:
            task.cancel()
        self.gather_all.result()

    def init_semaphore(self):
        """Return wrapper for coroutine"""
        if self.njobs:
            self.sema = asyncio.BoundedSemaphore(self.njobs)
            return self.await_with_semaphore
        return lambda coro: coro

    async def await_with_semaphore(self, coro):
        """Run coro with self.sema"""
        async with self.sema:
            return await coro
