import os
import sys
import logging

class FileStore:
    """Filesystem as key-value storage for string values.
    Similar interface can be provided by in-memory dictionary, Redis, etc.
    """

    def __contains__(self, key):
        """Does file exist?"""
        return os.path.lexists(key)

    def remove(self, key):
        """Remove file if it exists"""
        try:
            os.unlink(key)
        except FileNotFoundError:
            pass

    def get(self, key):
        """Read file contents as string, or return None"""
        try:
            with open(key) as file:
                return file.read()
        except FileNotFoundError:
            pass

    def __setitem__(self, key, value):
        """Write value as string to the file"""
        with open(key, 'w') as file:
            file.write(str(value))

    def mtime(self, key):
        """Return file modification time of key or None if it doesn't exist"""
        try:
            return os.stat(key, follow_symlinks=False).st_mtime
        except FileNotFoundError:
            pass

filestore = FileStore()
