from collections import namedtuple
import time

class Store:
    """Dictionary which keeps modification time of its values"""
    Entry = namedtuple('Entry', 'value date')
    def __init__(self):
        self.dict = {}
    def __contains__(self, key):
        return key in self.dict
    def __setitem__(self, key, value):
        self.dict[key] = Store.Entry(value, time.time())
    def __getitem__(self, key):
        return self.dict[key].value
    def date(self, key):
        """Return time when value was inserted/modified, or None if key is missing"""
        entry = self.dict.get(key)
        return entry and entry.date
    def get(self, key, default=None):
        """Like dict.get()"""
        try:
            return self[key]
        except KeyError:
            return default
    def remove(self, key):
        """Remove key if it's present"""
        try:
            del self.dict[key]
        except KeyError:
            pass