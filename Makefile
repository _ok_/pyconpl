# RULE SYNTAX
OUTPUT: INPUTS
	# indentation with TAB
	RECIPE

# $@ is substituted with output
x:
	sleep 0.5 && echo 17 > $@
y:
	sleep 0.5 && echo -14 > $@
z:
	sleep 0.5 && echo 3 > $@

# all combinations of (x, y, z)
x⋅x: x x
y⋅y: y y
z⋅z: z z
x⋅y: x y
y⋅x: y x
x⋅z: x z
y⋅z: y z
z⋅x: z x
z⋅y: z y

# $+ is like ' '.join(inputs)
x⋅x y⋅y z⋅z x⋅y y⋅x x⋅z y⋅z z⋅x z⋅y:
	sleep 0.5 && paste -d'*' $+ | bc > "$@"

(x+y+z)²: x⋅x y⋅y z⋅z x⋅y y⋅x x⋅z y⋅z z⋅x z⋅y
	sleep 0.5 && paste -d'+' $+ | bc > "$@"
